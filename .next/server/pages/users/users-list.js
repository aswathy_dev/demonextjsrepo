"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/users/users-list";
exports.ids = ["pages/users/users-list"];
exports.modules = {

/***/ "./pages/users/users-list.js":
/*!***********************************!*\
  !*** ./pages/users/users-list.js ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ UserList),\n/* harmony export */   \"getServerSideProps\": () => (/* binding */ getServerSideProps)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-jsx/style */ \"styled-jsx/style\");\n/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ \"next/head\");\n/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var next_script__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/script */ \"next/script\");\n/* harmony import */ var next_script__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_script__WEBPACK_IMPORTED_MODULE_3__);\n\n\n\n\nfunction UserList({ stars  }) {\n    return(/*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n        className: \"jsx-babf7251ad16d10e\" + \" \" + \"container\",\n        children: [\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((next_head__WEBPACK_IMPORTED_MODULE_2___default()), {\n                children: [\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"title\", {\n                        className: \"jsx-babf7251ad16d10e\",\n                        /*#__PURE__*/ children: \"Users List\"\n                    }, void 0, false, {\n                        fileName: \"/Users/imac1/Documents/AS_ReactApps/NEXTJSSTUDY/MyCreations/nextjs-blog/pages/users/users-list.js\",\n                        lineNumber: 8,\n                        columnNumber: 13\n                    }, this),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"script\", {\n                        src: \"https://connect.facebook.net/en_US/sdk.js\",\n                        className: \"jsx-babf7251ad16d10e\"\n                    }, void 0, false, {\n                        fileName: \"/Users/imac1/Documents/AS_ReactApps/NEXTJSSTUDY/MyCreations/nextjs-blog/pages/users/users-list.js\",\n                        lineNumber: 9,\n                        columnNumber: 13\n                    }, this)\n                ]\n            }, void 0, true, {\n                fileName: \"/Users/imac1/Documents/AS_ReactApps/NEXTJSSTUDY/MyCreations/nextjs-blog/pages/users/users-list.js\",\n                lineNumber: 7,\n                columnNumber: 9\n            }, this),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((next_script__WEBPACK_IMPORTED_MODULE_3___default()), {\n                src: \"https://connect.facebook.net/en_US/sdk.js\",\n                strategy: \"lazyOnload\",\n                onLoad: ()=>console.log(`script loaded correctly, window.FB has been populated`)\n            }, void 0, false, {\n                fileName: \"/Users/imac1/Documents/AS_ReactApps/NEXTJSSTUDY/MyCreations/nextjs-blog/pages/users/users-list.js\",\n                lineNumber: 11,\n                columnNumber: 9\n            }, this),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"main\", {\n                className: \"jsx-babf7251ad16d10e\",\n                children: [\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"h1\", {\n                        className: \"jsx-babf7251ad16d10e\",\n                        children: \"USERS\"\n                    }, void 0, false, {\n                        fileName: \"/Users/imac1/Documents/AS_ReactApps/NEXTJSSTUDY/MyCreations/nextjs-blog/pages/users/users-list.js\",\n                        lineNumber: 19,\n                        columnNumber: 9\n                    }, this),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"img\", {\n                        src: \"/images/profile.jpg\",\n                        height: 144,\n                        width: 144,\n                        alt: \"Your Name\",\n                        className: \"jsx-babf7251ad16d10e\"\n                    }, void 0, false, {\n                        fileName: \"/Users/imac1/Documents/AS_ReactApps/NEXTJSSTUDY/MyCreations/nextjs-blog/pages/users/users-list.js\",\n                        lineNumber: 20,\n                        columnNumber: 9\n                    }, this),\n                    stars.data.map((star, i)=>{\n                        return(/*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n                            className: \"jsx-babf7251ad16d10e\",\n                            children: [\n                                /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"h1\", {\n                                    className: \"jsx-babf7251ad16d10e\",\n                                    children: star.name\n                                }, void 0, false, {\n                                    fileName: \"/Users/imac1/Documents/AS_ReactApps/NEXTJSSTUDY/MyCreations/nextjs-blog/pages/users/users-list.js\",\n                                    lineNumber: 27,\n                                    columnNumber: 15\n                                }, this),\n                                star.email\n                            ]\n                        }, i, true, {\n                            fileName: \"/Users/imac1/Documents/AS_ReactApps/NEXTJSSTUDY/MyCreations/nextjs-blog/pages/users/users-list.js\",\n                            lineNumber: 26,\n                            columnNumber: 13\n                        }, this));\n                    })\n                ]\n            }, void 0, true, {\n                fileName: \"/Users/imac1/Documents/AS_ReactApps/NEXTJSSTUDY/MyCreations/nextjs-blog/pages/users/users-list.js\",\n                lineNumber: 18,\n                columnNumber: 9\n            }, this),\n            (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((styled_jsx_style__WEBPACK_IMPORTED_MODULE_1___default()), {\n                id: \"babf7251ad16d10e\",\n                children: \".container.jsx-babf7251ad16d10e{background:#fafafa;\\nmin-height:100vh;\\npadding:0 0.5rem;\\ndisplay:-webkit-box;\\ndisplay:-webkit-flex;\\ndisplay:-ms-flexbox;\\ndisplay:flex;\\n-webkit-flex-direction:column;\\n-ms-flex-direction:column;\\nflex-direction:column;\\n-webkit-justify-content:top;\\njustify-content:top;\\n-webkit-align-items:left;\\n-webkit-box-align:left;\\n-ms-flex-align:left;\\nalign-items:left}\"\n            }, void 0, false, void 0, this)\n        ]\n    }, void 0, true, {\n        fileName: \"/Users/imac1/Documents/AS_ReactApps/NEXTJSSTUDY/MyCreations/nextjs-blog/pages/users/users-list.js\",\n        lineNumber: 6,\n        columnNumber: 9\n    }, this));\n};\nasync function getServerSideProps(context) {\n    // const res = await fetch('https://api.github.com/repos/vercel/next.js')\n    const res = await fetch('https://gorest.co.in/public/v1/users');\n    const json = await res.json();\n    console.log('JSON', json);\n    return {\n        props: {\n            stars: json\n        }\n    };\n}\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy91c2Vycy91c2Vycy1saXN0LmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7OztBQUE0QjtBQUNJO0FBRWpCLFFBQVEsQ0FBQ0UsUUFBUSxDQUFDLENBQUMsQ0FBQ0MsS0FBSyxFQUFDLENBQUMsRUFBRSxDQUFDO0lBQ3pDLE1BQU0sNkVBQ0RDLENBQUc7a0RBQVcsQ0FBVzs7d0ZBQ3pCSixrREFBSTs7Z0dBQ0FLLENBQUs7O2dEQUFDLENBQVU7Ozs7OztnR0FDaEJDLENBQU07d0JBQUNDLEdBQUcsRUFBQyxDQUEyQzs7Ozs7Ozs7Ozs7Ozt3RkFFMUROLG9EQUFNO2dCQUNITSxHQUFHLEVBQUMsQ0FBMkM7Z0JBQy9DQyxRQUFRLEVBQUMsQ0FBWTtnQkFDckJDLE1BQU0sTUFDTkMsT0FBTyxDQUFDQyxHQUFHLEVBQUUscURBQXFEOzs7Ozs7d0ZBR3JFQyxDQUFJOzs7Z0dBQ0pDLENBQUU7O2tDQUFDLENBQUs7Ozs7OztnR0FDUkMsQ0FBRzt3QkFBQ1AsR0FBRyxFQUFDLENBQXFCO3dCQUFFUSxNQUFNLEVBQUUsR0FBRzt3QkFDM0NDLEtBQUssRUFBRSxHQUFHO3dCQUFFQyxHQUFHLEVBQUMsQ0FBVzs7Ozs7OztvQkFHMUJkLEtBQUssQ0FBQ2UsSUFBSSxDQUFDQyxHQUFHLEVBQUVDLElBQUksRUFBRUMsQ0FBQyxHQUFLLENBQUM7d0JBQzVCLE1BQU0sNkVBQ0hqQixDQUFHOzs7NEdBQ0RTLENBQUU7OzhDQUFFTyxJQUFJLENBQUNFLElBQUk7Ozs7OztnQ0FDYkYsSUFBSSxDQUFDRyxLQUFLOzsyQkFGSEYsQ0FBQzs7Ozs7b0JBS2YsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQlQsQ0FBQztBQUVNLGVBQWVHLGtCQUFrQixDQUFDQyxPQUFPLEVBQUUsQ0FBQztJQUMvQyxFQUF5RTtJQUN6RSxLQUFLLENBQUNDLEdBQUcsR0FBRyxLQUFLLENBQUNDLEtBQUssQ0FBQyxDQUFzQztJQUM5RCxLQUFLLENBQUNDLElBQUksR0FBRyxLQUFLLENBQUNGLEdBQUcsQ0FBQ0UsSUFBSTtJQUMzQmxCLE9BQU8sQ0FBQ0MsR0FBRyxDQUFDLENBQU0sT0FBRWlCLElBQUk7SUFDeEIsTUFBTSxDQUFDLENBQUM7UUFDTEMsS0FBSyxFQUFFLENBQUM7WUFBQzFCLEtBQUssRUFBRXlCLElBQUk7UUFBQyxDQUFDO0lBQ3pCLENBQUM7QUFDSixDQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vcGFnZXMvdXNlcnMvdXNlcnMtbGlzdC5qcz9iMTZlIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBIZWFkIGZyb20gJ25leHQvaGVhZCdcbmltcG9ydCBTY3JpcHQgZnJvbSAnbmV4dC9zY3JpcHQnXG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIFVzZXJMaXN0KHsgc3RhcnMgfSkge1xuICAgIHJldHVybiAoXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XG4gICAgICAgIDxIZWFkPlxuICAgICAgICAgICAgPHRpdGxlPlVzZXJzIExpc3Q8L3RpdGxlPlxuICAgICAgICAgICAgPHNjcmlwdCBzcmM9XCJodHRwczovL2Nvbm5lY3QuZmFjZWJvb2submV0L2VuX1VTL3Nkay5qc1wiIC8+XG4gICAgICAgIDwvSGVhZD5cbiAgICAgICAgPFNjcmlwdFxuICAgICAgICAgICAgc3JjPVwiaHR0cHM6Ly9jb25uZWN0LmZhY2Vib29rLm5ldC9lbl9VUy9zZGsuanNcIlxuICAgICAgICAgICAgc3RyYXRlZ3k9XCJsYXp5T25sb2FkXCJcbiAgICAgICAgICAgIG9uTG9hZD17KCkgPT5cbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGBzY3JpcHQgbG9hZGVkIGNvcnJlY3RseSwgd2luZG93LkZCIGhhcyBiZWVuIHBvcHVsYXRlZGApXG4gICAgICAgICAgICB9XG4gICAgICAgIC8+XG4gICAgICAgIDxtYWluPlxuICAgICAgICA8aDE+VVNFUlM8L2gxPlxuICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvcHJvZmlsZS5qcGdcIiAgaGVpZ2h0PXsxNDR9IFxuICAgICAgICB3aWR0aD17MTQ0fSBhbHQ9XCJZb3VyIE5hbWVcIiAvPlxuXG4gICAgICAgIHsvKiA8ZGl2Pk5leHQgc3RhcnM6IHtzdGFycy5kYXRhWzBdLm5hbWV9PC9kaXY+ICovfVxuICAgICAgICB7c3RhcnMuZGF0YS5tYXAoKHN0YXIsIGkpID0+IHtcbiAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPGRpdiBrZXk9e2l9PlxuICAgICAgICAgICAgICA8aDE+e3N0YXIubmFtZX08L2gxPlxuICAgICAgICAgICAgICB7c3Rhci5lbWFpbH1cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICApXG4gICAgICAgIH0pfVxuXG4gICAgICAgIDwvbWFpbj5cbiAgICAgICAgPHN0eWxlIGpzeD57YFxuICAgICAgICAgLmNvbnRhaW5lciB7XG4gICAgICAgICAgIGJhY2tncm91bmQ6ICNmYWZhZmE7XG4gICAgICAgICAgIG1pbi1oZWlnaHQ6IDEwMHZoO1xuICAgICAgICAgICBwYWRkaW5nOiAwIDAuNXJlbTtcbiAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgICAganVzdGlmeS1jb250ZW50OiB0b3A7XG4gICAgICAgICAgIGFsaWduLWl0ZW1zOiBsZWZ0O1xuXG4gICAgICAgICB9XG4gICAgICAgYH08L3N0eWxlPlxuICAgICAgICA8L2Rpdj5cbiAgICApICAgXG59XG5cbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBnZXRTZXJ2ZXJTaWRlUHJvcHMoY29udGV4dCkge1xuICAgIC8vIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKCdodHRwczovL2FwaS5naXRodWIuY29tL3JlcG9zL3ZlcmNlbC9uZXh0LmpzJylcbiAgICBjb25zdCByZXMgPSBhd2FpdCBmZXRjaCgnaHR0cHM6Ly9nb3Jlc3QuY28uaW4vcHVibGljL3YxL3VzZXJzJylcbiAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzLmpzb24oKVxuICAgIGNvbnNvbGUubG9nKCdKU09OJywganNvbilcbiAgICByZXR1cm4ge1xuICAgICAgIHByb3BzOiB7IHN0YXJzOiBqc29uIH1cbiAgICB9XG4gfVxuXG4iXSwibmFtZXMiOlsiSGVhZCIsIlNjcmlwdCIsIlVzZXJMaXN0Iiwic3RhcnMiLCJkaXYiLCJ0aXRsZSIsInNjcmlwdCIsInNyYyIsInN0cmF0ZWd5Iiwib25Mb2FkIiwiY29uc29sZSIsImxvZyIsIm1haW4iLCJoMSIsImltZyIsImhlaWdodCIsIndpZHRoIiwiYWx0IiwiZGF0YSIsIm1hcCIsInN0YXIiLCJpIiwibmFtZSIsImVtYWlsIiwiZ2V0U2VydmVyU2lkZVByb3BzIiwiY29udGV4dCIsInJlcyIsImZldGNoIiwianNvbiIsInByb3BzIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/users/users-list.js\n");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/***/ ((module) => {

module.exports = require("next/head");

/***/ }),

/***/ "next/script":
/*!******************************!*\
  !*** external "next/script" ***!
  \******************************/
/***/ ((module) => {

module.exports = require("next/script");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "styled-jsx/style":
/*!***********************************!*\
  !*** external "styled-jsx/style" ***!
  \***********************************/
/***/ ((module) => {

module.exports = require("styled-jsx/style");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/users/users-list.js"));
module.exports = __webpack_exports__;

})();