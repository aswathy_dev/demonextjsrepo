import Head from 'next/head'
import Script from 'next/script'

export default function UserList({ stars }) {
    return (
        <div className="container">
        <Head>
            <title>Users List</title>
            <script src="https://connect.facebook.net/en_US/sdk.js" />
        </Head>
        <Script
            src="https://connect.facebook.net/en_US/sdk.js"
            strategy="lazyOnload"
            onLoad={() =>
            console.log(`script loaded correctly, window.FB has been populated`)
            }
        />
        <main>
        <h1>USERS</h1>
        <img src="/images/profile.jpg"  height={144} 
        width={144} alt="Your Name" />

        {/* <div>Next stars: {stars.data[0].name}</div> */}
        {stars.data.map((star, i) => {
          return (
            <div key={i}>
              <h1>{star.name}</h1>
              {star.email}
          </div>
          )
        })}

        </main>
        <style jsx>{`
         .container {
           background: #fafafa;
           min-height: 100vh;
           padding: 0 0.5rem;
           display: flex;
           flex-direction: column;
           justify-content: top;
           align-items: left;

         }
       `}</style>
        </div>
    )   
}

export async function getServerSideProps(context) {
    // const res = await fetch('https://api.github.com/repos/vercel/next.js')
    const res = await fetch('https://gorest.co.in/public/v1/users')
    const json = await res.json()
    console.log('JSON', json)
    return {
       props: { stars: json }
    }
 }

